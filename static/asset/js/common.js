import $ from './jquery-3.2.1.min';
import util from './src/util/util';

window.$ = $;
window.jQuery = $;

const extend = $.extend;
const proxy = $.proxy;
const arrayProto = Array.prototype;
const objectProto = Object.prototype;
const hasOwn = objectProto.hasOwnProperty;
const arraySlice = arrayProto.slice;
const FUNCTION = "function";
const STRING = "string";
const NUMBER = "number";
const OBJECT = "object";
const NULL = "null";
const BOOLEAN = "boolean";
const UNDEFINED = "undefined";
const slice = [].slice;
;((global, $, undefined)=>{
    "use strict";
    global.LIB_NAME = 'DiepUi';
    const LIB_NAME = global.LIB_NAME || 'DiepUi';
    const doc = global.document;
	if (global[LIB_NAME]) return;
	const core = global[LIB_NAME] || (global[LIB_NAME] = {});
    function Class() {}

    if(!String.prototype.replaceAll){
        String.prototype.replaceAll = function(org, dest) {
            return this.split(org).join(dest);
        }
    }

    Class.extend = function(proto) {
        var base = function() {},
            member,
            _ = this,
            subclass = proto && proto.init ? proto.init : function () {
                _.apply(this, arguments);
            },
            fn;

        base.prototype = _.prototype;
        fn = subclass.fn = subclass.prototype = new base();

        for (member in proto) {
            if (proto[member] != null && proto[member].constructor === Object) {
                // Merge object members
                fn[member] = extend(true, {}, base.prototype[member], proto[member]);
            } else {
                fn[member] = proto[member];
            }
        }

        fn.constructor = subclass;
        subclass.extend = _.extend;

        return subclass;
    };

    extend(core, util, {
        name: LIB_NAME,
        version: '1.0.0',
        noop: function() {},
        emptyFn: function() {},
        widgets : core.widgets || [],
        _widgetRegisteredCallbacks: [],
        ui : core.ui || {},
        Class : Class
    });
    extend($.fn, {
        handler: function(handler) {
            this.data("handler", handler);
            return this;
        },
        emulateTransitionEnd : function (duration) {
            var called = false
            var $el = this
            $(this).one('bsTransitionEnd', function () { called = true })
            var callback = function () { if (!called) $($el).trigger($.support.transition.end);}
            setTimeout(callback, duration);
            return this;
        },
        /**
         * 같은 레벨에 있는 다른 row에서 on를 제거하고 현재 row에 on 추가
         * @function
         * @name $#activeItem
         * @param {string} className='on' 활성 클래스명
         * @return {jQuery}
         */
        activeItem : function (className, isActive) {
            className = className || 'active';
            if (typeof isActive === 'undefined') {
                isActive = true;
            }
            return this.toggleClass(className, isActive).siblings().toggleClass(className, !isActive).end();
        }
    });
    const Observable = Class.extend({
        init: function() {
            this._events = {};
        },

        bind: function(eventName, handlers, one) {
            var _ = this,
                idx,
                eventNames = typeof eventName === STRING ? [eventName] : eventName,
                length,
                original,
                handler,
                handlersIsFunction = typeof handlers === FUNCTION,
                events;

            if (handlers === undefined) {
                for (idx in eventName) {
                    _.bind(idx, eventName[idx]);
                }
                return _;
            }

            for (idx = 0, length = eventNames.length; idx < length; idx++) {
                eventName = eventNames[idx];

                handler = handlersIsFunction ? handlers : handlers[eventName];

                if (handler) {
                    if (one) {
                        original = handler;
                        handler = function() {
                            _.unbind(eventName, handler);
                            original.apply(_, arguments);
                        };
                        handler.original = original;
                    }
                    events = _._events[eventName] = _._events[eventName] || [];
                    events.push(handler);
                }
            }

            return _;
        },

        one: function(eventNames, handlers) {
            return this.bind(eventNames, handlers, true);
        },

        first: function(eventName, handlers) {
            var _ = this,
                idx,
                eventNames = typeof eventName === STRING ? [eventName] : eventName,
                length,
                handler,
                handlersIsFunction = typeof handlers === FUNCTION,
                events;

            for (idx = 0, length = eventNames.length; idx < length; idx++) {
                eventName = eventNames[idx];

                handler = handlersIsFunction ? handlers : handlers[eventName];

                if (handler) {
                    events = _._events[eventName] = _._events[eventName] || [];
                    events.unshift(handler);
                }
            }

            return _;
        },

        trigger: function(eventName, e) {
            var _ = this,
                events = _._events[eventName],
                idx,
                length;

            if (events) {
                e = e || {};

                e.sender = _;

                e._defaultPrevented = false;

                e.preventDefault = preventDefault;

                e.isDefaultPrevented = isDefaultPrevented;

                events = events.slice();

                for (idx = 0, length = events.length; idx < length; idx++) {
                    events[idx].call(_, e);
                }

                return e._defaultPrevented === true;
            }

            return false;
        },

        unbind: function(eventName, handler) {
            var _ = this,
                events = _._events[eventName],
                idx;

            if (eventName === undefined) {
                _._events = {};
            } else if (events) {
                if (handler) {
                    for (idx = events.length - 1; idx >= 0; idx--) {
                        if (events[idx] === handler || events[idx].original === handler) {
                            events.splice(idx, 1);
                        }
                    }
                } else {
                    _._events[eventName] = [];
                }
            }

            return _;
        }
    });

    const Widget = Observable.extend( {
        init: function(element, options) {
            var _ = this;
            _.element = $(element).handler(_);
            
            Observable.fn.init.call(_);

            var dataSource = options ? options.dataSource : null;
            var props;

            if (options) {
                props = (_.componentTypes || {})[(options || {}).componentType];
            }
            if (dataSource) {
                // avoid deep cloning the data source
                options = extend({}, options, { dataSource: {} });
            }

            options = _.options = extend(true, {}, _.options, _.defaults, props || {}, options);

            if (dataSource) {
                options.dataSource = dataSource;
            }

            _.element.data(options.name, _);

            _.bind(_.events, options);
        },

        events: [],

        options: {
            prefix: ""
        },

        _hasBindingTarget: function() {
            return !!this.element[0].bindingTarget;
        },

        _tabindex: function(target) {
            target = target || this.wrapper;

            var element = this.element,
                TABINDEX = "tabindex",
                tabindex = target.attr(TABINDEX) || element.attr(TABINDEX);

            element.removeAttr(TABINDEX);

            target.attr(TABINDEX, !isNaN(tabindex) ? tabindex : 0);
        },

        setOptions: function(options) {
            this._setEvents(options);
            $.extend(this.options, options);
        },

        _setEvents: function(options) {
            var _ = this,
                idx = 0,
                length = _.events.length,
                e;

            for (; idx < length; idx ++) {
                e = _.events[idx];
                if (_.options[e] && options[e]) {
                    _.unbind(e, _.options[e]);
                    if (_._events && _._events[e]) {
                        delete _._events[e];
                    }
                }
            }

            _.bind(_.events, options);
        },

        resize: function(force) {
            console.log(0)
            var size = this.getSize(),
                currentSize = this._size;
                
            if (force || (size.width > 0 || size.height > 0) && (!currentSize || size.width !== currentSize.width || size.height !== currentSize.height)) {
                this._size = size;
                this._resize(size, force);
                this.trigger("resize", size);
            }
        },

        getSize: function() {
            return core.dimensions(this.element);
        },

        size: function(size) {
            if (!size) {
                return this.getSize();
            } else {
                this.setSize(size);
            }
        },

        setSize: $.noop,
        _resize: $.noop,

        destroy: function() {
            var _ = this;

            _.element.removeData(_.options.prefix + _.options.name);
            _.element.removeData("handler");
            _.unbind();
        },
        _destroy: function() {
            this.destroy();
        }
    });
    extend(core.ui, {
        Widget: Widget,
        Observable : Observable,
        roles: {},
        plugin: function(widget, register, globalEvent) {
            var name = widget.fn.name;
            var prefix = "";
            register = register || core.ui;
            register[name] = widget;

            register.roles[name.toLowerCase()] = widget;
            var widgetEntry = { name: name, widget: widget, prefix: prefix || "" };
            core.widgets.push(widgetEntry);
            for (var i = 0, len = core._widgetRegisteredCallbacks.length; i < len; i++) {
                core._widgetRegisteredCallbacks[i](widgetEntry);
            }

            $.fn[name] = function(options) {
                var value = this,
                    args;
                if (typeof options === STRING) {
                    this.each(function(){
                        var widget = $.data(this, name),
                            method,
                            result;
                        method = widget[options];
                        result = method.apply(widget, args);
                    });
                } else {
                    this.each(function() {
                        var $this = $(this)
                        var data  = $this.data(name)
                        if (!data) $this.data(name, (data = new widget(this, options)))
                    });
                }
                return value.data(name);
            };
            $.fn[name].Constructor = widget;
            if(globalEvent){
                globalEvent();
            }
        },
        /**
         * 딤 생성
         * @param STRING
         */
        showDim : function(target){
            var root = $('body');
            var targetName = target || '';
            var dim = $('<div id="bgDim" class="cm_dim">');
            if(target) dim.attr('data-target', targetName)
			root.append(dim);
			setTimeout(()=>{
				dim.addClass('dim_show');
				root.addClass('dim_show');
			},0)
        },
        /**
         * 딤 제거
         * @param $el
         */
		hideDim : function(){
			var root = $('body');
			root.removeClass('dim_show');
			setTimeout(()=>{
				$('#bgDim').remove();
			},0)
        },
        /**
         * 스크롤이벤트 무효화
         * @param $el
         */
        disableScroll: function ($el) {
            $el = $el || $win;

            var scrollTop = $el.scrollTop();
            $el.on("scroll.disableScroll mousewheel.disableScroll DOMMouseScroll.disableScroll touchmove.disableScroll", function (event) {
                event.preventDefault();
                $el.scrollTop(scrollTop);
            });
        },

        /**
         * 스크롤이벤트 무효화 취소
         * @param $el
         */
        enableScroll: function ($el) {
            $el = $el || $win;

            $el.off(".disableScroll");
        },
    });
    
    // (new MutationObserver(mutations => {
    //     const updates = [];
    //     mutations.forEach(mutation => applyMutation(mutation, updates));
    //     // console.log(updates)
    //     updates.forEach(el => {
    //         // console.log(el)
    //     });
    // })).observe(document, {
    //     childList: true,
    //     subtree: true,
    //     characterData: true,
    //     attributes: true
    // });
    // var strPrototype = String.prototype;
    // var startsWithFn = strPrototype.startsWith || function (search) { return this.lastIndexOf(search, 0) === 0; };

    // function startsWith(str, search) {
    //     return startsWithFn.call(str, search);
    // }

    // function applyMutation(mutation, updates) {

    //     const {target, type} = mutation;

    //     const update = type !== 'attributes'
    //         ? applyChildList(mutation)
    //         : applyAttribute(mutation);

    //     if (update && !updates.some(element => element.contains(target))) {
    //         updates.push(target.contains ? target : target.parentNode); // IE 11 text node does not implement contains
    //     }

    // }

    // function applyAttribute({target, attributeName}) {
        
    //     if (attributeName !== 'data-options' && attributeName !== 'data-modules' ) {
    //         return false;
    //     }
    //     console.log(attributeName)

    //     const name = getComponentName(attributeName);

    //     if (!name || !(name in UIkit)) {
    //         return;
    //     }
        
    //     if (hasAttr(target, attributeName)) {
    //         UIkit[name](target);
    //         return true;
    //     }

    //     const component = UIkit.getComponent(target, name);

    //     if (component) {
    //         component.$destroy();
    //         return true;
    //     }

    // }

    // function applyChildList({addedNodes, removedNodes}) {

    //     for (let i = 0; i < addedNodes.length; i++) {
    //         // apply(addedNodes[i], connect);
    //     }

    //     for (let i = 0; i < removedNodes.length; i++) {
    //         // apply(removedNodes[i], disconnect);
    //     }

    //     return true;
    // }

    // function apply(node, fn) {

    //     if (!isElement(node)) {
    //         return;
    //     }
    
    //     fn(node);
    //     node = node.firstElementChild;
    //     while (node) {
    //         const next = node.nextElementSibling;
    //         apply(node, fn);
    //         node = next;
    //     }
    // }

    // function getComponentName(attribute) {
    //     return startsWith(attribute, 'uk-') || startsWith(attribute, 'data-uk-')
    //         ? camelize(attribute.replace('data-uk-', '').replace('uk-', ''))
    //         : false;
    // }

    // function camelize(str) {
    //     return str.replace(camelizeRe, toUpper);
    // }
})(window, jQuery);





























