/**
 * @property type = alert(default), confirm 	//메세지 박스 타입 설정
 * @property message = ""(default)				//노출 될 메세지 문자열
 * @property defaultBtn = '<button type="button" id="customMsgSubmit" class="btn btn-primary">확인</button>'(default)	// 기본 버튼
 * @property cancelBtn : '<button type="button" id="customMsgCancel" class="btn btn-success">취소</button>'			// 취소 버튼
 * @callback defaultCallback = 확인 버튼 클릭 시 콜백
 * @callback cancelCallback =  취소 버튼 클릭 시 콜백
 */
export default ((core, $)=>{
	core.customAlert = function(obj){
		let DEFAULTS = {
				root : $('body'),
				type : 'alert',
				size : "modal-sm",
				title : null,
				defaultBtn : '<button type="button" id="customMsgSubmit" class="btn">확인</button>',
				cancelBtn : '<button type="button" id="customMsgCancel" class="btn">취소</button>',
				message : "",
				defaultCallback : null,
				cancelCallback : null,
				dismissCallback : null,
				btnClicked : false
			},
			options = $.extend({}, DEFAULTS, obj);
		// 버튼이 텍스트만 전달받을 경우 버튼으로 만들어줌.
		options.defaultBtn = (options.defaultBtn.indexOf('<') != -1) ? 
			options.defaultBtn : '<button type="button" id="customMsgSubmit" class="btn">'+options.defaultBtn+'</button>';
		options.cancelBtn = (options.cancelBtn.indexOf('<') != -1) ? 
			options.cancelBtn : '<button type="button" id="customMsgCancel" class="btn">'+options.cancelBtn+'</button>';

		// 모달 구조 생성
		let	btns = (options.type.indexOf('confirm') != -1) ? options.cancelBtn + options.defaultBtn : options.defaultBtn,
			modal = '<div id="customAlertModal" class="modal '+options.type+'" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">\
				<div class="modal-dialog '+options.size+'" role="document">\
					<div class="modal-content">';

			// 타이틀이 있을 경우 구조 생성
			modal += (options.title !== null)? '<div class="modal-header">\
					<p class="modal-title">'+options.title+'</p>\
				</div>' : '';
			
			// 기본 모달컨텐츠 생성
			modal += 	'<div class="modal-body">'+options.message+'</div>\
						<div class="modal-footer">'+btns+'</div>\
					</div>\
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>\
				</div>\
			</div>';
		
		if(options.backdrop === 'static'){
			$(modal).modal({backdrop: 'static', keyboard: false})
			// 기본 버튼 클릭 시 콜백
			.on('click', '#customMsgSubmit', function (e) {
				const $this = $(this);
				let callback;
				options.btnClicked = true;
				if(options.defaultCallback) callback = options.defaultCallback(e);
				if (callback === false) return false;
				$this.closest('[role="dialog"]').modal('hide');
				// console.log(e)
			})
			// 취소 버튼 클릭 시 콜백
			.on('click', '#customMsgCancel', function (e) {
				const $this = $(this);
				let callback;
				options.btnClicked = true;
				if(options.cancelCallback) callback = options.cancelCallback(e);
				if (callback === false) return false;
				$this.closest('[role="dialog"]').modal('hide');
			})
			.on('hidden.bs.modal', function (e) {
				let callback;
				if(!options.btnClicked && options.dismissCallback) callback = options.dismissCallback(e);
				$('#customAlertModal').remove();
			})
		} else {
			$(modal).modal('show')
			// 기본 버튼 클릭 시 콜백
			.on('click', '#customMsgSubmit', function (e) {
				const $this = $(this);
				let callback;
				options.btnClicked = true;
				if(options.defaultCallback) callback = options.defaultCallback(e);
				if (callback === false) return false;
				$this.closest('[role="dialog"]').modal('hide');
				// console.log(e)
			})
			// 취소 버튼 클릭 시 콜백
			.on('click', '#customMsgCancel', function (e) {
				const $this = $(this);
				let callback;
				options.btnClicked = true;
				if(options.cancelCallback) callback = options.cancelCallback(e);
				if (callback === false) return false;
				$this.closest('[role="dialog"]').modal('hide');
			})
			.on('hidden.bs.modal', function (e) {
				let callback;
				if(!options.btnClicked && options.dismissCallback) callback = options.dismissCallback(e);
				$('#customAlertModal').remove();
			})
		}
	}
})(window[LIB_NAME], jQuery);