/**
 * @name BarChart
 */
const win = $(window);
const Default = {
	padding : 48,
	breakPoint : null
};
const activeClass = 'active';
const NAME = "barChart";

export default (function(core, $){
	const ui = core.ui;
	const Widget = ui.Widget;
	const BarChart = Widget.extend({
		name : NAME,
		init : function (element, config){
			const _ = this;
			const options = _.options = $.extend({}, Default, config);
			_.element = $(element);
			Widget.fn.init.call(_, element, options);
			_.options.dataWrap = _.element.find('.chart_draw');
			_.options.scroll;
			if(_.element.data('active')){
				_.chartUpdate();
			}else{
				_._bindEvents();
			}			
		},
		_bindEvents : function (){
            const _ = this;
			_._drawChart();
			win.on('load.chartview scroll.chartview', function(){
				const $this = $(this);
				if(_._visibleCheck() && !_.element.hasClass(activeClass)){
					_.chartUpdate();
					win.off('load.chartview scroll.chartview')
				}
			});
			$('a[data-toggle="tab"]').on('shown.bs.tab.chartview', function (e) {
				if(_._visibleCheck() && !_.element.hasClass(activeClass)){
					_.chartUpdate();
					win.off('load.chartview scroll.chartview');
				}
			})
		},
		_visibleCheck : function (){
			const _ = this;
			let callback = false;
			let elVisible =  _.element[0].offsetWidth !== 0 && _.element[0].offsetHeight !== 0;
			let scrHeight = win.height();
			let elPos = _.element.offset().top + _.element.height();
			let scrollTop = win.scrollTop();
			if( (scrollTop+scrHeight) > elPos && elVisible ){
				callback = true;
			}
			return callback;
		},
		_drawChart : function (){
			const _ = this;
			let el = '';
			let data = _.options.dataset;
			let currentIndex = _.options.currentIndex;
			for (let i = 0; i < data.length; i++) {
				if(i == currentIndex){
					el +='<span class="bar current">';	
				}else{
					el +='<span class="bar">';
				}
				el +='<i class="bar1" style="height:0%"></i>';
				el +='<i class="bar2" style="height:0%"></i>';
				el +='<span class="label">'+data[i].label+'</span>';
				el +='</span>';
			}
			_.options.dataWrap.html(el);
		},
		chartUpdate : function (){
			const _ = this;
			let data = _.options.dataset;
			_.element.addClass(activeClass);
			_.options.dataWrap.find('.bar').each(function(n){
				let $this = $(this),
					bar1 = $this.find('.bar1'),
					bar2 = $this.find('.bar2');
				data[n].position = $this.position().left;
				bar2.transition({ height: data[n].data[1]+'%' }, 400, function(){
					bar1.transition({ height: data[n].data[0]+'%', delay:200}, 400, function(){
						_.options.scroll.mCustomScrollbar('scrollTo', _.options.dataset[_.options.currentIndex].position);			
					});
				});
			})
			_.options.scroll = $('.chart_draw_wrap').mCustomScrollbar({
				"theme":"dark-thin",
				"horizontalScroll": true,
				"setLeft": "0px"
			});
			_.element.data('active', true);
			
		},
		_getXInc : function (width, maxNum){
			return width / (maxNum-1)
		},
		scrollTo : function (n){
			const _ = this.data('barChart');
			_.options.scroll.mCustomScrollbar('scrollTo', _.options.dataset[n].position);
		}
	})
	ui.plugin(BarChart)
})(window[LIB_NAME], jQuery);