/**
 * @decs 화면에 사이즈에 따라 html 클래스 분기처리
 * @decs changeSize 커스텀 이벤트
 * @param e 이벤트 타입
 * @param mode 해상도 mode
 * $(window).on('changeSize', function(e, mode){
		console.log(mode)
	})
 */



export default ((core, $)=>{
	"use strict";
	var r = document.querySelector('html'),
		w = $(window).width(),
		$html = $('html'),
		$event = $.event,
		$special,
		sizes = core.detect.sizes,
		$special = $event.special.changeSize = {
			setup: function() {
				$( this ).on( "resize", $special.handler );
			},
			teardown: function() {
				$( this ).off( "resize", $special.handler );
			},
			handler: function( event, execAsap ) {
				var context = this,
					w = $(window).width(),
					mode = core.detect.mediaInfo.mode,
					dispatch = function(mode) {
						event.type = "changeSize";
						$(window).trigger('changeSize', mode);
					};
				for (var i = 0; sizes[i]; i++) {
					if (w >= sizes[i].min && w <= sizes[i].max && core.detect.mediaInfo.mode !== sizes[i].mode) {
						dispatch(sizes[i].mode);
						core.detect.mediaInfo.mode = sizes[i].mode;
						break;
					}
				}
			}
		};
	for (var i = 0; sizes[i] ; i++) {
		if (w >= sizes[i].min && w <= sizes[i].max) {
			switch (sizes[i].mode) {
				case 'mobile':
					r.classList.add('mobile')
					break;
				case 'tablet':
					r.classList.add('tablet')
					break;
				case 'web':
					r.classList.add('web')
					break;
			}
			core.detect.mediaInfo.mode = sizes[i].mode;
			break;
		}
	}
	$(window).on('changeSize', function(e, mode){
		for (let i = 0; i < sizes.length; i++) {
			$html.removeClass(sizes[i].mode);
		}
		$html.addClass(mode);
		core.detect.mediaInfo.mode = mode;
	})

	// transition end event
	$.support.transition = core.transitionEnd()
    if (!$.support.transition) return

    $.event.special.bsTransitionEnd = {
		bindType: $.support.transition.end,
		delegateType: $.support.transition.end,
		handle: function (e) {
			if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
		}
    }

})(window[LIB_NAME], jQuery);