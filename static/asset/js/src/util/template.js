/**
 * @name Template
 */
const win = $(window);
const forEach = Array.prototype.forEach;
const extend = $.extend;
const proxy = $.proxy;
const compilePart = function(part, stringPart) {
	if (stringPart) {
		return "'" +
			part.split("'").join("\\'")
				.split('\\"').join('\\\\\\"')
				.replace(/\n/g, "\\n")
				.replace(/\r/g, "\\r")
				.replace(/\t/g, "\\t") + "'";
	} else {
		var first = part.charAt(0),
			rest = part.substring(1);
		if (first === "=") {
			return "+(" + rest + ")+";
		} else if (first === ":") {
			return "+$htmlEncode(" + rest + ")+";
		} else {
			return ";" + part + ";$outPut+=";
		}
	}
};
export default (function(core, $){

    // html 텝플릿 생성
    var argumentNameRegExp = /^\w+/,
        encodeRegExp = /\$\{([^}]*)\}/g,
        escapedCurlyRegExp = /\\\}/g,
        curlyRegExp = /__CURLY__/g,
        escapedSharpRegExp = /\\#/g,
        sharpRegExp = /__SHARP__/g,
        Template = {
            paramName: "data", // name of the parameter of the generated template
            useWithBlock: true, // whether to wrap the template in a with() block
            render: function(template, data) {
                var idx,
                    length,
                    html = "";
    
                for (idx = 0, length = data.length; idx < length; idx++) {
                    html += template(data[idx]);
                }
    
                return html;
            },
            compile: function(template, options) {
                var settings = extend({}, this, options),
                    paramName = settings.paramName,
                    argumentName = paramName.match(argumentNameRegExp)[0],
                    useWithBlock = settings.useWithBlock,
                    functionBody = "var $outPut, $htmlEncode = DiepUi.htmlEncode;",
                    fn,
                    parts,
                    idx;
    
                if (core.isFunction(template)) {
                    return template;
                }
    
                functionBody += useWithBlock ? "with(" + paramName + "){" : "";
    
                functionBody += "$outPut=";
    
                parts = template
                    .replace(escapedCurlyRegExp, "__CURLY__")
                    .replace(encodeRegExp, "#=$htmlEncode($1)#")
                    .replace(curlyRegExp, "}")
                    .replace(escapedSharpRegExp, "__SHARP__")
                    .split("#");
    
                for (idx = 0; idx < parts.length; idx ++) {
                    functionBody += compilePart(parts[idx], idx % 2 === 0);
                }
    
                functionBody += useWithBlock ? ";}" : ";";
    
                functionBody += "return $outPut;";
    
                functionBody = functionBody.replace(sharpRegExp, "#");
                try {
                    fn = new Function(argumentName, functionBody);
                    fn._slotCount = Math.floor(parts.length / 2);
                    return fn;
                } catch(e) {
                    throw new Error(kendo.format("Invalid template:'{0}' Generated code:'{1}'", template, functionBody));
                }
            }
		};

	extend(core, {
        Template: Template,
        template: proxy(Template.compile, Template),
		render: proxy(Template.render, Template)
	})
})(window[LIB_NAME], jQuery);