// 기본 해상도 분기
const DEFAULT_SCREEN_SIZE = [
	{
		mode: 'mobile',
		min: 0,
		max: 767
	},
	{
		mode: 'tablet',
		min: 768,
		max: 1024
	},
	{
		mode: 'web',
		min: 1025,
		max: 3840
	}
];
const doc = window.document;
const tmpInput = doc.createElement('input');
const isMobile = ('orientation' in global) || global.IS_MOBILE === true;
const supportPlaceholder = ('placeholder' in tmpInput);
const namespace = function(name, obj, ctx) {
	if (typeof name !== 'string') {
		obj && (name = obj);
		return name;
	}

	var root = ctx || global,
		names = name.split('.'),
		i, item;

	if (names[0] === LIB_NAME) {
		names = names.slice(1);
	}

	for (i = -1; item = names[++i];) {
		root = root[item] || (root[item] = {});
	}

	return extend(root, obj || {});
};
exports.namespace = namespace;

/**
 * core 하위에 name에 해당하는 네임스페이스를 생성하여 object를 설정해주는 함수
 *
 * @function
 * @name core.addon
 *
 * @param {string} name .를 구분자로 해서 core을 시작으로 하위 네임스페이스를 생성. name이 없으면 core에 추가된다.
 * @param {Object|Function} obj
 *
 * @example
 * core.addon('urls', {
 *    store: 'Store',
 *    company: 'Company'
 * });
 *
 * alert(core.urls.store);
 * alert(core.urls.company);
 */
const addon = function(name, object, isExecFn) {
	if (typeof name !== 'string') {
		object = name;
		name = '';
	}

	var root = core,
		names = name ? name.split('.') : [],
		ln = names.length - 1,
		leaf = names[ln];

	if (isExecFn !== false && typeof object === 'function' && !hasOwn.call(object, 'superClass')) {
		object = object.call(root);
	}

	for (var i = 0; i < ln; i++) {
		root = root[names[i]] || (root[names[i]] = {});
	}

	return (leaf && (root[leaf] ? extend(root[leaf], object) : (root[leaf] = object))) || extend(root, object), object;
};
exports.addon = addon;

/**
 * @readonly
 * @name core.detect
 * @enum {*}
 * @property {boolean} mediaInfo // 현재 해상도별 구분 객체
 * @property {boolean} isTouch // 터치디바이스 여부
 * @property {boolean} isRetina // 레티나 여부
 * @property {boolean} isMobile // orientation 작동여부로 판단
 * @property {boolean} isMac // 맥OS
 * @property {boolean} isLinux // 리눅스
 * @property {boolean} isWin // 윈도우즈
 * @property {boolean} is64Bit // 64비트 플랫폼
 * @property {boolean} isIE // IE
 * @property {boolean} ieVersion // IE의 버전
 * @property {boolean} isOpera // 오페라
 * @property {boolean} isChrome // 크롬
 * @property {boolean} isSafari // 사파리
 * @property {boolean} isWebKit // 웹킷
 * @property {boolean} isGecko // 파이어폭스
 * @property {boolean} isIETri4 // IE엔진
 * @property {boolean} isAir // 어도비 에어
 * @property {boolean} isIOS // 아이폰, 아이패드
 * @property {boolean} isAndroid // 안드로이드
 * @property {number} iosVersion // ios 버전 : [8, 1, 0] -> [major, minor, revision]
 * @property {number} androidVersion // android 버전 : [4, 1, 0] -> [major, minor, revision]
 * @example
 * if(core.browser.isIE && core.browser.isVersion < 9) {
 *     alert('구버전을 사용하고 있습니다.');
 * }
 */
const detect = (function() {
	var detect = {},
		win = global,
		na = win.navigator,
		ua = na.userAgent,
		lua = ua.toLowerCase(),
		match;
	detect.mediaInfo = function(){};
	detect.mediaInfo.mode = null;
	detect.sizes = DEFAULT_SCREEN_SIZE;
	detect.placeholder = supportPlaceholder;
	detect.isStrict = (typeof global == 'undefined');

	detect.isRetina = 'devicePixelRatio' in global && global.devicePixelRatio > 1;
	detect.isAndroid = lua.indexOf('android') !== -1;
	detect.isBadAndroid = /Android /.test(na.appVersion) && !(/Chrome\/\d/.test(na.appVersion));
	detect.isOpera = !!(win.opera && win.opera.buildNumber);
	detect.isWebKit = /WebKit/.test(ua);
	detect.isTouch = !!('ontouchstart' in global);
	detect.isMobileDevice = ('ontouchstart' in win) || win.DocumentTouch && document instanceof DocumentTouch || na.msMaxTouchPoints || false;

	match = /(msie) ([\w.]+)/.exec(lua) || /(trident)(?:.*rv.?([\w.]+))?/.exec(lua) || ['', null, -1];
	detect.isIE = !detect.isWebKit && !detect.isOpera && match[1] !== null;
	detect.version = detect.ieVersion = parseInt(match[2], 10);
	detect.isOldIE = detect.isIE && detect.version < 9;

	detect.isWin = (na.appVersion.indexOf("Win") != -1);
	detect.isMac = (ua.indexOf('Mac') !== -1);
	detect.isLinux = (na.appVersion.indexOf("Linux") != -1);
	detect.is64Bit = (lua.indexOf('wow64') > -1 || (na.platform === 'Win64' && lua.indexOf('x64') > -1));

	detect.isChrome = (ua.indexOf('Chrome') !== -1);
	detect.isGecko = (ua.indexOf('Firefox') !== -1);
	detect.isAir = ((/adobeair/i).test(ua));
	detect.isIOS = /(iPad|iPhone)/.test(ua);
	detect.isSafari = !detect.isChrome && (/Safari/).test(ua);
	detect.isIETri4 = (detect.isIE && ua.indexOf('Trident/4.0') !== -1);
	detect.isGalaxy = (ua.indexOf(' SHV-') !== -1);

	detect.msPointer = !!(na.msPointerEnabled && na.msMaxTouchPoints && !win.PointerEvent);
	detect.pointer = !!((win.PointerEvent && na.pointerEnabled && na.maxTouchPoints) || detect.msPointer);

	if (detect.isAndroid) {
		detect.androidVersion = function() {
			var v = ua.match(/[a|A]ndroid[^\d]*(\d+).?(\d+)?.?(\d+)?/);
			if (!v) {
				return -1;
			}
			return [parseInt(v[1] | 0, 10), parseInt(v[2] | 0, 10), parseInt(v[3] | 0, 10)];
		}();
	} else if (detect.isIOS) {
		detect.iosVersion = function() {
			var v = ua.match(/OS (\d+)_?(\d+)?_?(\d+)?/);
			return [parseInt(v[1] | 0, 10), parseInt(v[2] | 0, 10), parseInt(v[3] | 0, 10)];
		}();
	}

	detect.isMobile = isMobile || detect.isIOS || detect.isAndroid;
	
	return detect;
}());
exports.detect = detect;


/**
 * 주어진 시간내에 호출이 되면 무시되고, 초과했을 때만 비로소 fn를 실행시켜주는 함수
 * @param {Function} fn 콜백함수
 * @param {number} time 딜레이시간
 * @param {*} scope 컨텍스트
 * @returns {Function}
 * @example
 * // 리사이징 중일 때는 #box의 크기를 변경하지 않다가,
 * // 리사이징이 끝나고 0.5초가 지난 후에 #box사이즈를 변경하고자 할 경우에 사용.
 * $(window).on('resize', core.delayRun(function(){
 *
$('#box').css('width', $(window).width());
	*  }, 500));
	*/
const delayRun = function(fn, time, scope) {
	time || (time = 250);
	var timeout = null;
	var runner = function() {
		var args = [].slice.call(arguments),
			self = this;
		runner.cancel();
		timeout = setTimeout(function() {
			fn.apply(scope || self, args);
			timeout = null;
		}, time);
	};
	runner.cancel = function() {
		clearTimeout(timeout);
		timeout = null;
	};
	return runner;
};
exports.delayRun = delayRun;

/**
 * 주어진 시간내에 호출이 되면 무시되고, 초과했을 때만 비로소 fn를 실행시켜주는 함수
 * @function
 * @name core.throttle
 * @param {function} fn 콜백함수
 * @param {number} time 딜레이시간
 * @param {*} scope 컨텍스트
 * @returns {function}
 * @example
 * // 리사이징 중일 때는 #box의 크기를 변경하지 않다가,
 * // 리사이징이 끝나고 0.5초가 지난 후에 #box사이즈를 변경하고자 할 경우에 사용.
 * $(window).on('resize', core.throttle(function(){
 *		$('#box').css('width', $(window).width());
	*  }, 500));
	*/
const throttle = function(fn, time, scope) {
	time || (time = 250);
	var lastCall = 0;
	return function() {
		var now = +new Date();
		if (now - lastCall < time) {
			return;
		}
		lastCall = now;
		fn.apply(scope || this, arguments);
	};
};
exports.throttle = throttle;

/**
 * 주어진 값을 배열로 변환
 * @function
 * @name core.toArray
 * @param {*} value 배열로 변환하고자 하는 값
 * @return {array}
 *
 * @example
 * core.toArray('abcd"); // ["a", "b", "c", "d"]
 * core.toArray(arguments);  // arguments를 객체를 array로 변환하여 Array에서 지원하는 유틸함수(slice, reverse ...)를 쓸수 있다.
 */
const toArray = function(value) {
	try {
		return arraySlice.apply(value, arraySlice.call(arguments, 1));
	} catch (e) {}

	var ret = [];
	try {
		for (var i = 0, len = value.length; i < len; i++) {
			ret.push(value[i]);
		}
	} catch (e) {}
	return ret;
};
exports.toArray = toArray;

/**
 * 15자의 영문, 숫자로 이루어진 유니크한 값 생성
 * @function
 * @name core.getUniqId
 * @return {string}
 */
const getUniqId = function(len) {
	len = len || 32;
	var rdmString = "";
	for (; rdmString.length < len; rdmString += Math.random().toString(36).substr(2));
	return rdmString.substr(0, len);
};
exports.getUniqId = getUniqId;

/**
 * 순번으로 유니크값 을 생성해서 반환
 * @function
 * @name core.nextSeq
 * @return {number}
 */
const nextSeq = (function() {
	var seq = 0;
	return function(prefix) {
		return (prefix || '') + (seq += 1);
	};
}());
exports.nextSeq = nextSeq;

/**
 * add, remove 클래스
 * @name core.tog
 * @param {Boolean} v 
 * 
 */
const tog = function(v){
	return v ? 'addClass' : 'removeClass';
}
exports.tog = tog;

const isFunction = function(fn) {
	return typeof fn === "function";
}
exports.isFunction = isFunction;

/**
 * 객체로 전환 하고자 하는 JSON 스트링을 객체로 변환
 * @function
 * @name core.stringToObject
 * @param {JSON String} value 객체로 변환 하고자 하는 JSON 스트링
 * @return {Object}
 */
const stringToObject = function (str){
	if (typeof str === "object") return str;
	var str = str.replace(/\s/g, '');
	var obj;
	obj = JSON.parse(str);
	return obj;
}
exports.stringToObject = stringToObject;

// html 인코딩
let ampRegExp = /&/g,
	ltRegExp = /</g,
	quoteRegExp = /"/g,
	aposRegExp = /'/g,
	gtRegExp = />/g;
const htmlEncode = function (value) {
	return ("" + value).replace(ampRegExp, "&amp;").replace(ltRegExp, "&lt;").replace(gtRegExp, "&gt;").replace(quoteRegExp, "&quot;").replace(aposRegExp, "&#39;");
}
exports.htmlEncode = htmlEncode;

const dimensions = function(element, dimensions) {
	var domElement = element[0];

	if (dimensions) {
		element.css(dimensions);
	}

	return { width: domElement.offsetWidth, height: domElement.offsetHeight };
};
exports.dimensions = dimensions;


const showDim = function() {
	console.log('dddd')
};
exports.showDim = showDim;

const  transitionEnd = function() {
    var el = document.createElement('div')

    var transEndEventNames = {
		WebkitTransition : 'webkitTransitionEnd',
		MozTransition    : 'transitionend',
		OTransition      : 'oTransitionEnd otransitionend',
		transition       : 'transitionend'
    }

    for (var name in transEndEventNames) {
		if (el.style[name] !== undefined) {
			return { end: transEndEventNames[name] }
		}
    }

    return false
}
exports.transitionEnd = transitionEnd;

const getParameter = function(name) {  
	var rtnval = '';  
	var nowAddress = unescape(location.href);  
	var parameters = (nowAddress.slice(nowAddress.indexOf('?') + 1,  
		nowAddress.length)).split('&');  
	for (var i = 0; i < parameters.length; i++) {  
		var varName = parameters[i].split('=')[0];  
		if (varName.toUpperCase() == name.toUpperCase()) {  
			rtnval = parameters[i].split('=')[1];  
			break;  
		}  
	}  
	return rtnval;  
}  
exports.getParameter = getParameter;


const cssLiteralToObject  = function(str){
	var strValue = str, arr = strValue.split(';'), i = arr.length-1, obj = {},objArr,objFirst,objLast,objName,objProp,arrFirst,arrLast,firstStr, lastStr;
	for ( ; i >= 0; i--) {
		if(arr[i] !== ""){
			if( arr[i].indexOf('{') !== -1 || arr[i].indexOf('}') !== -1 || arr[i].indexOf('[') !== -1 || arr[i].indexOf(']') !== -1 ){
				objFirst = arr[i].indexOf('{');
				objLast = arr[i].lastIndexOf('}');
				arrFirst = arr[i].indexOf('[');
				arrLast = arr[i].lastIndexOf(']');
				// console.log(arr[i])
				if(objFirst > arrFirst && arrFirst !== -1 ){
					firstStr = arrFirst;
					lastStr = arrLast;
				}else{
					firstStr = objFirst;
					lastStr = objLast;
				}
				objName = arr[i].slice(0, firstStr-2);
				objProp = arr[i].slice(firstStr, lastStr+1).replaceAll('\'', '\"');
				obj[objName] = DiepUi.stringToObject(objProp);
			}else{
				objArr = arr[i].replaceAll(" ", "").split(':');
				if(objArr.length !== 2 || objArr[1] == "" ){
					throw new Error('옵션값이 잘못되었습니다.\n 잘못 전달된 옵션 속성 : '+strValue);
				}
				obj[objArr[0]] = objArr[1];
			}
		}
	}
	return obj
}
exports.cssLiteralToObject = cssLiteralToObject;