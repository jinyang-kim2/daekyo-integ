/**
 * @name sticky
 * @selector [data-modules="stickyBox"]'
 * @options breakPoint : $(element) 	// 전달된 셀렉터 의 포지션 하단에서 브레이크
 */
const win = $(window);
const forEach = Array.prototype.forEach;
const Default = {
	padding : 0,
    breakPoint : null,
    items : null
};
const activeClass = 'fixed';
const NAME = "cascadingGrid";

export default (function(core, $){
	const ui = core.ui;
	const Widget = ui.Widget;
	const CascadingGrid = Widget.extend({
        name : NAME,
		init : function (element, config){
            const _ = this;
            const options = _.options = $.extend({}, Default, config);
			_.element = $(element);
			Widget.fn.init.call(_, element, options);
            _.mount();
            win.on('resize', function(){
                _.mount();
            })
        },
        mount : function (){
            const _ = this;
            _.element.width('auto');
            _.items = _.element.find('[data-items]');
            let containerWidth = _.element.outerWidth(),
                itemWidth = _.items[0].getBoundingClientRect().width + _.options.padding,
                cols = Math.max(Math.floor( (containerWidth+_.options.padding) / (itemWidth)), 1),
                count = 0,
                itemsPosX = [],
                itemsGutter = [];
            // console.log(containerWidth+wrapPadding)
            _.element.css({"width":(itemWidth * cols - _.options.padding) + 'px'});
            
            for ( var g = 0 ; g < cols ; ++g ) {
                itemsPosX.push(g * itemWidth + _.options.padding);
                itemsGutter.push(_.options.padding);
            }
            forEach.call(_.items, function (item, i) {
                var itemIndex = itemsGutter
                    .slice(0)
                    .sort(function (a, b) {
                        return a - b;
                    })
                    .shift();
                itemIndex = itemsGutter.indexOf(itemIndex);
                var posX = parseInt(itemsPosX[itemIndex]) - _.options.padding;
                var posY = parseInt(itemsGutter[itemIndex]) - _.options.padding;

                item.style.transform = 'translate(' + posX + 'px,' + posY + 'px)';

                itemsGutter[itemIndex] += item.getBoundingClientRect().height + _.options.padding;
                count = count + 1;
            });
            var containerHeight = itemsGutter
            .slice(0)
            .sort(function (a, b) {
                return a - b;
            })
            .pop();
            _.element.height(containerHeight)
            
        }
	})
    ui.plugin(CascadingGrid);
    win.on('load', function () {
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var el = $('[data-modules="cascadingGrid"]');
            if(el.data('cascadingGrid') !== undefined) el.cascadingGrid('mount');
		})
	});
})(window[LIB_NAME], jQuery);