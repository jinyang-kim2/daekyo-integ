/**
 * @name sticky
 * @selector [data-modules="stickyBox"]'
 * @options breakPoint : $(element) 	// 전달된 셀렉터 의 포지션 하단에서 브레이크
 */
const win = $(window);
const Default = {
	padding : 0,
	breakPoint : null,
	name : "sticky"
};
const activeClass = 'fixed';
const name = "sticky"

export default (function(core, $){
	const ui = core.ui;
	const Widget = ui.Widget;
	const Sticky = Widget.extend({
		name : name,
		init: function (element, config){
			const _ = this;
			const options = _.options = $.extend({}, Default, config);
			Widget.fn.init.call(_, element, options);
			_.options.padding = Number(_.options.padding);
			_.options.pos = _.element.offset().top + _.options.padding;
			_._bindEvent();
		},
		_bindEvent : function() {
			const _ = this;
			win.on('scroll', function(e){
				e.preventDefault();
				const $this = $(this);
				let scrollPos = $this.scrollTop();
				// console.log(scrollPos)
				// console.log(_.element.offset().top )
				if(scrollPos > _.options.pos){
					_.element.addClass(activeClass);
					if(_.options.breakPoint !== null){
						_._breakAction(scrollPos);
					}
				}else{
					_.element.removeClass(activeClass);
				}
			})
			.on('load', function(){
				_.posRefresh();
			})
			
			
		},
		posRefresh : function(pos){
			const _ = this;
			_.options.pos = _.element.offset().top + _.options.padding;
		},
		_breakAction : function(pos) {
			const _ = this;
			
			let breakPointPos = $(_.options.breakPoint).offset().top + $(_.options.breakPoint).outerHeight() + pos;
			let elPos = _.element.offset().top + _.element.outerHeight() + pos;
			if(breakPointPos < elPos){
				_.element.find(".sticky_inner").css({"transform":"translate(0, "+(breakPointPos - elPos)+"px)"});
			}else{
				_.element.find(".sticky_inner").removeAttr('style');
			}
		}
	})
	ui.plugin(Sticky);
})(window[LIB_NAME], jQuery);