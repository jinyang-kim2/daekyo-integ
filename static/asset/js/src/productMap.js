/**
 * @name productMap
 */
const win = $(window);
const Default = {
	padding : 48,
	breakPoint : null
};
const forEach = Array.prototype.forEach;
const activeClass = 'active';
const NAME = "productMap";
const NUM = /[^0-9]/g;

export default (function(core, $){
	const ui = core.ui;
	const Widget = ui.Widget;
	
	const BarChart = Widget.extend({
		name : NAME,
		init : function (element, config){
			const _ = this;
			const options = _.options = $.extend({}, Default, config);
			_.titles = {};
			_.titles.inner;
			_.sticky;
			_.element = $(element);
			_.innerTable = _.element.find('.inner_table');
			Widget.fn.init.call(_, element, options);
			_.element.data('activeHorizontal', null)
			_.element.data('activeVertical', null)
			_._createThead();
			_._createTbody();
			_._bindEvents();
			_._setListIndex();
		},
		_bindEvents : function (){
            const _ = this;
			win.on('resize load', function(){
				const $this = $(this);
				for (const header in _.titles) {
					if (_.titles.hasOwnProperty(header)) {
						_.setResizing(_.titles[header]);
					}
				}
				setTimeout(function(){
					_.sticky.posRefresh();
				}, 500)
				
			});
			_.innerTable.on('scroll', function(){
				_.titles.inner.css('transform', 'translate(-'+$(this).scrollLeft()+'px, 0)')
			})
			_.titles.thead.on('click', 'button', function(){
				const $el = $(this).parent('.th')
				let index = $el.index();
				if($el.hasClass(activeClass)){
					$el.removeClass(activeClass);
					index = null;
				}else{
					$el.activeItem();
				}
				_.element.data('activeVertical', index)
				_.activeLists();
			})
			_.titles.tbody.on('click', 'button', function(){
				// if()
				console.log('ddd')
				const $el = $(this).parent('.th')
				let index = $el.index();
				if($el.hasClass(activeClass)){
					$el.removeClass(activeClass);
					index = null;
				}else{
					$el.activeItem();
				}
				_.element.data('activeHorizontal', index)
				_.activeLists();
			})
		},
		_createThead : function(){
			const _ = this;
			const thead = _.element.find('thead th');
			const wrap = $('<div id="'+NAME+'tableHeader" class="table_header" />')
			const stickyInner = $('<div class="sticky_inner" />')
			const wrapInner = $('<div class="title_inner" />')
			
			let pos = _._getPostion(thead);
			let el = _._getChildren(thead);
			forEach.call(el, function(el, i){
				let th = $('<div class="th">');
				th.css({
					'width':pos[i].width +'px',
					'height':pos[i].height +'px',
					'top':pos[i].top + 'px',
					'left':pos[i].left + 'px'
				});
				if(pos[i].class !== null) $(th).addClass(pos[i].class);
				th.append(el);
				wrapInner.append(th);
			})
			_.titles.inner = wrapInner;
			
			stickyInner.append(wrapInner);
			wrap.append(stickyInner);
			_.element.prepend(wrap)
			_.sticky = wrap.sticky({
				breakPoint:'#mapTable'
			});
			_.titles.thead = wrap;
			
		},
		_createTbody : function(){
			const _ = this;
			const tbody = _.element.find('tbody th');
			const wrap = $('<div id="'+NAME+'tbodTitle" class="table_title" />')
			let pos = _._getPostion(tbody);
			let el = _._getChildren(tbody);
			forEach.call(el, function(el, i){
				let th = $('<div class="th">');
				th.css({
					'top':pos[i].top +'px',
					'left':pos[i].left +'px',
					'width':pos[i].width +'px',
					'height':pos[i].height +'px'
				});
				if(pos[i].class !== null) $(th).addClass(pos[i].class);
				th.append(el);
				wrap.append(th);
			})
			_.element.append(wrap)
			_.titles.tbody = wrap;
			
		},
		setResizing : function(el){
			
			const _ = this;
			const header = el;
			const lists = header.find('.th');
			let pos;
			if(header.hasClass('title_inner')) return;
			if(header.hasClass('table_header')){
				pos = _._getPostion(_.element.find('thead th'));
			}else{
				pos = _._getPostion(_.element.find('tbody th'));
			}
			forEach.call(lists, function(el, i){
				let th = el;
				$(th).css({
					'top':pos[i].top +'px',
					'left':pos[i].left +'px',
					'width':pos[i].width +'px',
					'height':pos[i].height +'px'
				});
				
			})
		},
		_setListIndex : function(){
			const _ = this;
			const lists = _.element.find('.list_box');
			let dataIndex;
			forEach.call(lists, function(el, i){
				const listBar = lists.find('.lists');
				forEach.call(listBar, function(el, i){
					let classList = el.classList;
					dataIndex = [];
					for (let i = 0; i < classList.length; i++) {
						const n = classList[i].replace(NUM, '');
						if(n != ''){
							dataIndex.push(Number(n));
						}
					}
					$(el).data('map', dataIndex)
				})
			})
		},
		_getPostion : function(target){
			const _ = this;
			let posData = [];
			forEach.call(target, function (item, i) {
				// if(core.detect.isIOS){
				// 	posData[i] = {
				// 		// top:(core.detect.isIE)?item.offsetTop-1:item.offsetTop,
				// 		top:item.offsetTop,
				// 		left:item.offsetLeft,
				// 		width:item.offsetWidth,
				// 		height:item.offsetHeight-1
				// 	}
				// }else{
					posData[i] = {
						top:item.offsetTop,
						left:item.offsetLeft,
						width:item.offsetWidth,
						height:item.offsetHeight,
						class : item.getAttribute('class')
					}
				// }

			});
			return posData;
		},
		_getChildren : function(target){
			const _ = this;
			let posData = [];
			forEach.call(target, function (item, i) {
				posData[i] = $(item.children).clone();
			});
			return posData;
		},
		activeLists: function(){
			const _ = this;
			let activeHorizontal = _.element.data('activeHorizontal')
			let activeVertical = _.element.data('activeVertical')
			let lists = _.element.find('.list_box');
			forEach.call(lists, function(el, i){
				const listBar = $(el).find('.lists');
				if(activeHorizontal !== null && i !== activeHorizontal){
					listBar.addClass('disabled');
				}else{
					if(activeVertical !== null) {
						forEach.call(listBar, function(el, i){
							let dataIndex = $(el).data('map');
							if(dataIndex.indexOf(activeVertical) === -1){
								$(el).addClass('disabled')
							}else{
								$(el).removeClass('disabled')
							}
						})
					}else{
						listBar.removeClass('disabled');
					}
				}
			})
		}
	})
	ui.plugin(BarChart)
})(window[LIB_NAME], jQuery);