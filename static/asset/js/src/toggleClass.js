
const win = $(window);
const Default = {
	classLists : [],
};
const forEach = Array.prototype.forEach;
const name = "classesToggle"

/**
 * @name classesToggle
 * @selector [data-modules="classesToggle"]'
 */
export default (function(core, $){
	const ui = core.ui;
	const Widget = ui.Widget;
	const ClassesToggle = Widget.extend({
		name : name,
		init : function(element, config){
			const _ = this;
			const options = _.options = $.extend({}, Default, config);
			Widget.fn.init.call(_, element, options);
			_.element = $(element);
			_._bindEvents()
		},
		_bindEvents : function(){
			const _ = this;
			_.element.on("click", function(e){
				e.preventDefault();
				const $this = $(this)[0];
				let classes = $this.classList,
					count = _._checkClass(classes);
				if(_.options.classLists.length === 0) return false;
				if (_.options.classLists.length === 1){
					_._toggleClass($this);
				}else{
					if(count === _.options.classLists.length-1){
						$this.classList.remove(_.options.classLists[_.options.classLists.length-1]);
						$this.classList.add(_.options.classLists[0]);
					}else if(count === null){
						$this.classList.add(_.options.classLists[0]);
					}else{
						$this.classList.remove(_.options.classLists[count]);
						$this.classList.add(_.options.classLists[count+1]);	
					}
				}
			})
		},
		_toggleClass : function(el){
			const _ = this;
			el.classList.toggle(_.options.classLists[0]);
		},
		_checkClass : function(classLists){
			const _ = this;
			let count;
			for (let i = 0; i < classLists.length; i++) {
				for (let j = 0; j < _.options.classLists.length; j++) {
					if(classLists[i] == _.options.classLists[j]){
						count = j;
						break;
					}else{
						count = null;
					}
				}
			}
			return count;
		}
	})
	ui.plugin(ClassesToggle)
})(window[LIB_NAME], jQuery);