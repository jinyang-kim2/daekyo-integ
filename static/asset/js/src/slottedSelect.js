
const win = $(window);
const Default = {};
const forEach = Array.prototype.forEach;
const NAME = "slottedSelect"
/**
 * @name SlottedSelect
 * @selector [data-modules="slottedSelect"]'
 */
export default (function(core, $){
	const ui = core.ui;
	const Widget = ui.Widget;
	const SlottedSelect = Widget.extend({
		name : NAME,
		init : function(element, config){
			const _ = this;
			const options = _.options = $.extend({}, Default, config);
			_.element = $(element);
			Widget.fn.init.call(_, element, options);
			_.selector = _.element.find('.inner');
			_.moveIndex = -_.selector.find('.active').index();
			_.itemLength = -_.selector.find('li').length +1;
			_.itemHeight = _.element.find('button').height();
			_.selector.data('pos',_.moveIndex);
			_.movePos = _.itemHeight * _.moveIndex;
			_.selector.css('transform', 'translate(0px, '+_.movePos+'px)');
			_.scrolling;
			

			_.element
			.on('touchstart', function(e){e.preventDefault();_._dragStart($(this), e)})
			.on('mousedown', function(e){e.preventDefault();core.detect.isTouch || _._dragStart($(this), e)})
			.on('mousewheel', function(e){e.preventDefault();_._wheelScroll(e);})
			
			$(document).on('mousemove touchmove', function(e){e.preventDefault();!_.isDrag || _._dragMove(e)});
			$(document).on('mouseup', function(e){core.detect.isTouch || !_.isDrag || _._dragEnd(e)});
			$(document).on('touchend', function(e){!_.isDrag || _._dragEnd(e)});
		},
		_dragStart : function(el, e){
			const _ = this;
			_.isDrag = true;
			_.startPointY = _._getY(e);
		},
		_dragMove : function(e){
			const _ = this;
			let pos = _._getY(e) - _.startPointY + (_.itemHeight * _.selector.data('pos'));
			if(_.movePos >= 0 && pos>=0) {
				_._scrollMove(0);
				_.scrolling = false;
				_.moveIndex = 0;
				_._scrollEnd();
			}else if(_.movePos <= (_.itemHeight - _.selector.height()) && pos<=0){
				_._scrollMove(_.itemHeight - _.selector.height());
				_.scrolling = false;
				_.moveIndex = _.itemLength;
				_._scrollEnd();
			}else{
				_._scrollMove(_.movePos);
				_.scrolling = true;
			};
			_.movePos = pos;			
		},
		_dragEnd : function(e){
			const _ = this;
			_.isDrag = false;
			if(_.scrolling){
				_.moveIndex = (_.movePos / _.itemHeight).toFixed(0);
				_._scrollEnd();
			}			
		},
		_getY : function(e){
			let pos = 0;
			if (core.detect.isTouch) {
				pos = e.originalEvent.touches[0].clientY;
			}else{
				pos = e.clientY;
			}
			return pos;
		},
		_scrollMove : function(pos){
			const _ = this;
			TweenMax.to(_.selector, .3, {y: pos});
		},
		_scrollEnd : function(){
			const _ = this;
			
			let pos = _.itemHeight * _.moveIndex;
			let lists = _.selector.find('li');
			lists.removeClass('active');
			lists.eq(Math. abs(_.moveIndex)).addClass('active');
			_.selector.data('pos', _.moveIndex)
			TweenMax.to(_.selector, .3, {y: pos});
		},
		_wheelScroll : function(e){
			const _ = this;
			let wheelPos = e.wheelDelta ? evt.wheelDelta / 10 : (e.originalEvent.detail || e.originalEvent.deltaY);
			
			if(wheelPos >= 0){
				_.moveIndex-=1;
			}else{
				_.moveIndex+=1;
			}
			if(_.moveIndex > 0){
				_.moveIndex = 0;
			}else if(_.moveIndex < _.itemLength){
				_.moveIndex = _.itemLength;
			}
			_._scrollEnd();
		}
	})
	ui.plugin(SlottedSelect);
})(window[LIB_NAME], jQuery);