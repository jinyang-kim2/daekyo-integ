
const win = $(window);
const Default = {};
const forEach = Array.prototype.forEach;
const activeClass = 'active';
const setClassName = 'hidden_decs';
const lineHeight = 96;
const NAME = "imageView";
const openClass = 'modal-open';

/**
 * @name imageView
 * @selector [data-modules="imageView"]'
 */
export default (function(core, $){
	const ui = core.ui;
	const Widget = ui.Widget;
	const imageView = Widget.extend({
		name : NAME,
		init : function (element, config){
			const _ = this;
			const options = _.options = $.extend({}, Default, config);
			_.element = $(element);
			Widget.fn.init.call(_, element, options);
			_._bindEvents();
		},
		_bindEvents : function (){
			const _ = this;
			_.element.on('click', function(e){
				e.preventDefault();
				const $this = $(this);
				_._createImageView($this)
			})
		},
		_createImageView : function(el){
			const _ = this;
			const wrap = $('<div id="imageViewer" class="image_viewer" />');
			const dim = $('<div class="cm_dim dim_show" />')
			const span = $('<span class="img"></span>');
			const btn = $('<button type="button" class="close"><span>닫기</span></button>');
			const image = el.prev('img').clone();
			span.append(image).append(btn);
			wrap.append(span).append(dim);
			
			$('body').append(wrap).addClass(openClass);
			btn.one('click', function(){
				_._closeViewer(wrap, dim)
			});
			dim.one('click', function(event){
				_._closeViewer(wrap, btn)
			});
		},
		_closeViewer : function(target, off){
			target.remove();
			$('body').removeClass(openClass);
			off.off('click')
		}
	})
	ui.plugin(imageView);
})(window[LIB_NAME], jQuery);