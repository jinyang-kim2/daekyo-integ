import $ from '../jquery-3.2.1.min';
// import TweenMax from '../TweenMax.min';

/**
 * Header, Footer 공통
 * @
 */
const Default = {
	activeClass : "active",
	subMenuCheckClass : "hasSub",
	activeGnb : null,
	DURATIONS : 500
};
const doc = $(document);
const win = $(window);
const NAME = "LayoutAction";
const objectProto = Object.prototype;
const hasOwn = objectProto.hasOwnProperty;
const openClass = 'modal-open';
const LIBRAYLISTS = {
	slick : {
		el : '[data-modules="slick"]',
		name : "slick",
		default : {
			arrows:true,
			dots: true,
			infinite: false
		}
	},
	customScroll : {
		el : '[data-modules="customScroll"]',
		name : "mCustomScrollbar",
		default : {
			theme:"dark-thin",
			scrollInertia:500,
			advanced : {
				autoScrollOnFocus : ''
			}
			
		}
	},
	rangeChart : {
		el : '[data-modules="rangeChart"]',
		name : "rangeChart"
	},
	tooltip : {
		el : '[data-toggle="tooltip"]',
		name : "tooltip"
	},
	cascadingGrid : {
		el : '[data-modules="cascadingGrid"]',
		name : "cascadingGrid"
	},
	classesToggle : {
		el : '[data-modules="classesToggle"]',
		name : "classesToggle"
	},
	starRating : {
		el : '[data-modules="starRating"]',
		name : "starRating"
	},
	numberAnimate : {
		el : '[data-modules="numberAnimate"]',
		name : "numberAnimate",
		default : {
			duration: 1000
		}
	},
	sticky : {
		el : '[data-modules="stickyBox"]',
		name : "sticky"
	},
	overflowText : {
		el : '[data-modules="overflowText"]',
		name : "overflowText"
	},
	jumpTo : {
		el : '[data-modules="jumpTo"]',
		name : "jumpTo"
	},
	imageView : {
		el : '[data-modules="imageView"]',
		name : "imageView"
	}
	
}

export default (function(core, $){
	const ui = core.ui;
	const Widget = ui.Widget;
	const LayoutAction = Widget.extend({
		name : NAME,
		init : function (element, config){
			const _ = this;
			const options = _.options = $.extend({}, Default, config);
			_.element = $(element);
			_.header = $(element).find('#headers');
			_.gnb = _.header.find('.gnb');
			Widget.fn.init.call(_, element, options);
			_.options.progressBar = $('#progressBar');
			_.mediaInfo = core.detect.mediaInfo.mode
			_._bindEvents();
			_.currentImgLoaded(_.mediaInfo);

			if(_.element.find('.btn_top').length !== 0) _.topBtn();
			if(_.element.find('#bannerWrap').length !== 0) _.commonBanner();

			// hash 체크하여 
			if (window.location.hash !== ""){
				_.openTab();
			}

			win.on('hashchange', _.openTab)

			// 임시
			$(function(){
				// $('.global_search').on('click', function(){
				// 	var $this = $(this);
				// 	if($this.hasClass('active')){
				// 		$this.removeClass('active')
				// 		$('#searchLayer').hide()
				// 	}else{
				// 		$this.addClass('active')
				// 		$('#searchLayer').show()
				// 		$('#searchLayer').find('.layer_close').one('click', function(e){
				// 			$('#searchLayer').hide();
				// 			$this.removeClass('active')
				// 		})
				// 	}
				// 	return false;
				// });
			})
			// // 임시
			$('#selectType').find('.ctr').on('click', function(e){
				var $this = $(this);
				var target = $this.closest('.guide_list');
				var parent = $this.closest('.left_cont').length;
				if(parent){
					target.removeClass('right_select').toggleClass('left_select')
				}else{
					target.removeClass('left_select').toggleClass('right_select')
				}
				
				e.preventDefault();
			});
			
			$('.digital_namecard').on('click', '.namecard_btn.turn_ar', function(e){
				var $this = $(this);
				var target = $this.closest('.digital_namecard')
				target.toggleClass('active');
			});
		},
		// hideQuickMenu
		_bindEvents : function (){
			const _ = this;
			win.on('changeSize', function(e, mode){
				_.mediaInfo = mode;
				_.currentImgLoaded(mode);
			});
			win.on('load', function(e){
				_.footerFixed();
				_.initLibray();
				// setTimeout(function(){
					TweenMax.to($('#topWave g'), 2.3, {x: 70,duration: 3, ease: Quad.easeOut});
				// }, 500)
				$('.digital_namecard').each(function(){
					$(this).addClass('ready');
				})
				
			})

			// 22002747_디자인6
			
			win.on('resize', function(e){
				_.footerFixed();
			})

			if(_.element.find("#wrap").hasClass('main')){
				win.on('load', function(e){
					TweenMax.to($('#topWave g'), 2.3, {x: 70,duration: 3, ease: Quad.easeOut});
					
				})	
				win.on('scroll', function(e){
					var sctop = ($(this).scrollTop()/4)+70;
					if(sctop <=200){
						TweenMax.to($('#topWave g'), 1, {x: sctop,duration: 3, ease: Quad.easeOut});
					}
				});
			}else{
				win.on('scroll', function(e){
					if(_.options.progressBar.length !== 0){
						let percent = (window.scrollY / (document.body.clientHeight - window.innerHeight)) * 100;
						_.options.progressBar.find(">span").css({"width":percent+"%"})
					}
					var sctop = ($(this).scrollTop()/4)+70;
					if(sctop <=200){
						TweenMax.to($('#topWave g'), 1, {x: sctop,duration: 3, ease: Quad.easeOut});
					}
					if ($(this).scrollTop() > 100) {
						$('.btn_top').addClass('active');
					} else {
						$('.btn_top').removeClass('active');
					}
				});
			}

			_.element.on('click', '#bgDim', function(){
				const $this = $(this);
				let target = $this.data('target');
				if(!target) {
					core.ui.hideDim();
					return false;
				};
				// let method = "hide"+target;
				if (typeof _["hide"+target] !== 'function') throw new Error(target+" 메서드가 존재하지 않습니다.");
				_["hide"+target](target);
			})

			_.gnb.on('click', '> ul > li > a', function(e){
				e.preventDefault();
				var $this = $(this),
					target = $this.next('.deps'),
					parent = $this.parent();
				if(target.length !== 0){
					parent.activeItem('active');
					_.header.addClass('open_deps');
				}else{
					// _.header.removeClass('open_deps');
					location.href=$this.attr('href');
				}
			})
		},
		commonBanner : function(){
			const _ = this;
			let bannerWrap = _.element.find('#bannerWrap'),
				bannerSmallHeight = bannerWrap.find('.banner_img.min img').height(),
				bannerbigHeight = bannerWrap.find('.banner_img.detail img').height(),
				stickyEl = _.element.find('[data-modules="stickyBox"]'),
				evScroll = 'scroll.banner',
				margin = bannerSmallHeight - bannerbigHeight,
				evWheel = 'mousewheel.banner';
			$(window).on(evWheel, function(e){
				var wheelPos = e.wheelDelta ? evt.wheelDelta / 10 : (e.originalEvent.detail || e.originalEvent.deltaY);
				var scrollTop = $(this).scrollTop();
				if( window.getComputedStyle(bannerWrap[0]).display !== 'none' && scrollTop === 0 && wheelPos <=0 && !bannerWrap.hasClass('detail') ){		
					bannerWrap.addClass('detail');
					bannerWrap.find('.banners').slick('refresh');
					bannerWrap.css({"margin-top":margin+"px"});
					bannerWrap.animate({"margin-top":0}, 500, function(){
						moduleRefresh();
					});
				}
			});
			$(window).on(evScroll, function(){
				var scrollTop = $(this).scrollTop();
				if(bannerWrap.find('.banner_img.detail').outerHeight()-bannerSmallHeight < scrollTop && bannerWrap.hasClass('detail')){
					bannerWrap.removeClass('detail')
					bannerWrap.animate({"margin-top":margin+"px"}, 300, function(){
						moduleRefresh();
						bannerWrap.find('.banners').slick('refresh');
						bannerWrap.removeAttr('style');
						$(window).scrollTop(76);
					});
				}
			})
			bannerWrap.find('#closeBanner').on('click', function(){
				bannerWrap.find('.banners').slick('unslick');
				bannerWrap.remove();
				$(window).off(evScroll);
				$(window).off(evWheel)
				moduleRefresh();
				
			})
			function moduleRefresh(){
				if(stickyEl.length !== 0) {
					stickyEl.each(function(){
						if($(this).data('sticky')){
							$(this).sticky('posRefresh');
						}
					})
				}
			}
		},
		commonBannerShow : function(){
			const _ = this;
			let banner = _.element.find('#bannerWrap');
			if(banner.length !== 0 && _.mediaInfo !== 'mobile'){
				banner.show();
				banner.find('[data-modules="slick"]').slick('refresh')
			}
		},
		commonBannerHide : function(){
			const _ = this;
			let banner = _.element.find('#bannerWrap');
			if(banner.length !== 0 && _.mediaInfo !== 'mobile'){
				banner.hide();
			}
		},
		controllLayers : function(elm, layer){
			const _ = this;
			let el = elm;
			let target = layer;
			if(el.hasClass(_.options.activeClass)){
				_["hide"+target](target);
			}else{
				_["show"+target](target);
			}	
		},
		closeLayers : function(elm, layer){
			const _ = this;
			let el = elm;
			let target = layer;
			_["hide"+target](target);
			$('[data-callayer='+target+']').removeClass(_.options.activeClass);
			
		},
		showTotalSearch : function(str){
			const _ = this;
			let el = $('#searchLayer');
			$('body').addClass(openClass);
			el.show();
		},
		hideTotalSearch : function(str){
			const _ = this;
			let el = $('#searchLayer');
			$('body').removeClass(openClass);
			el.hide();
		},
		showAllMenu : function(str){
			const _ = this;
			let el = $('#allMenu')
			el.addClass('active')
			core.ui.showDim(str);
			
		},
		hideAllMenu : function(str){
			const _ = this;
			let el = $('#allMenu');
			el.removeClass('active');
			core.ui.hideDim(str);
		},
		showQuickMenu : function(str){
			const _ = this;
			let el = $('#addQuickMenu')
			el.addClass('active')
			// core.ui.showDim(str);
			
		},
		hideQuickMenu : function(str){
			const _ = this;
			let el = $('#addQuickMenu');
			el.removeClass('active');
			// core.ui.hideDim(str);
		},
		

	
		goToTop : function(){
			var top = doc.scrollTop();
			$(".topLink")[core.tog(top > 60)]("show");
		},


		openTab : function(){
			let selector = $('a[href="'+window.location.hash+'"]');
			if (selector.data('toggle') ==='tab') selector.tab('show');
		},

		setMinHeight : function(){
			const _ = this;
			var height = win.height(),
				lnb = _.options.header.find('.logo-wrap'),
				footer = $('#footer').parent(),
				container = $('#yuhs-container').parent(),
				posTop = (footer.length) ? footer.offset().top + footer.height() : null,
				minusHeight = (footer.find('.fixed-list').length !== 0) ? footer.find('.fixed-list').height() : 0;
			// if (posTop && height > posTop && container.length ) {
			// 	if(core.detect.mediaInfo.mode == 'web' && (_.element.hasClass('community-page') || _.element.hasClass('professor-page')) ){
			// 		$('#yuhs-container').css({"min-height" : (lnb.height() - container.offset().top - footer.height() ) +'px' })
			// 	}else{
			// 		$('#yuhs-container').css({"min-height" : container.height()+(height - posTop - minusHeight) +'px' })
			// 	}
				
			// }
		},

		footerFixed : function(){
			const _ = this;
			let winHeight = win.height(),
				footer = $('.wrap_footer'),
				bodyHeight = (footer.hasClass('fixed_footer'))? footer.outerHeight()+$('body').outerHeight():$('body').outerHeight();
				// console.log(_.mediaInfo)
			if(_.mediaInfo !== 'mobile' && winHeight > bodyHeight){
				footer.addClass('fixed_footer');
			}else if(footer.hasClass('fixed_footer')){
				footer.removeClass('fixed_footer');
			}

		},

		initLibray : function(){
			const _ = this;
			
			var LibrayLists = LIBRAYLISTS;
			for (const i in LibrayLists) {
				if (LibrayLists.hasOwnProperty(i)) {
					_.setOptions(LibrayLists[i]);
				}
			}
		},

		// modulesRefresh : function(conts){
		// 	const _ = this;
		// 	alert(0)
		// 	var moduleLists = [conts.find('[data-module]')],
		// 		moduleName;
		// 	for (let j = 0; j < moduleLists[i].length; j++) {
		// 		moduleName = (moduleLists[i][j].dataset.module) ? moduleLists[i][j].dataset.module : moduleLists[i][j].dataset.modules;
		// 		switch (moduleName) {
		// 			case "slick":{
		// 				$('[data-modules="slick"]').slick("setPosition");
		// 				break;
		// 			}
		// 			default : break;
		// 		}
				
		// 	}
		// },

		setOptions : function(obj){
			/**
			 * @options : enableMode ["web", "tablet", "mobile"];
			 */
			var el = $(obj.el),
				defaultOption = obj.default,
				name = obj.name;
			el.each(function(){
				
				var $this = $(this),
					option = $.extend({}, defaultOption),
					options = $this.data('options') ? $.extend(option, core.stringToObject($this.data('options'))): option,
					mode = core.detect.mediaInfo.mode,
					setMode = (!options.hasOwnProperty('enableMode')) || (function(options, mode){
						var checkMode = options['enableMode'];
						if (checkMode !== null) {
							checkMode = (checkMode.indexOf(mode) !== -1)? true : false;
							
						}else{
							checkMode = true;
						}
						return checkMode;
					})(options, mode);
				if(!$this.parents('pre').length /* 진행표 소스 보기 용. */ && setMode && !$this.data('active')) {
					$this[name](options);
					$this.data('active', true);	//플러그인 적용 활성화 체크
				}
			});
		},

		currentImgLoaded : function(mode){
			let d = mode,
				lists = [].slice.call(document.querySelectorAll('.current_img'));
			lists.forEach(el=>{
				let src = el.dataset[mode] || el.dataset['src'];
				el.setAttribute('src', src)
			});
		},

		setFooterPadding : function(){
			const _ = this;
			let mobileBanner = _.element.find('#mobileBanner'),
				wrapper = _.element.find('#wrap'),
				mobileMenu = _.element.find('#mobileMenu'),
				mobileBtn = _.element.find('#contents .sticky_btn');

			if(mobileBanner.length !== 0 /*&& window.getComputedStyle(mobileBanner[0]).display !== 'none'*/){
				wrapper.addClass('inner_banner');
				mobileBanner.one('click', '.close', function(){
					mobileBanner.remove();
					wrapper.removeClass('inner_banner');
				})
			}
			if(mobileMenu.length !== 0 /*&& window.getComputedStyle(mobileMenu[0]).display !== 'none'*/){
				wrapper.addClass('inner_mobilemenu');
			}
			if(mobileBtn.length !== 0 /*&& window.getComputedStyle(mobileBtn[0]).display !== 'none'*/){
				wrapper.addClass('inner_stickybtn');
			}
		},

		topBtn : function(){
			let btnPath = document.querySelector('.btn_top path');
			let btnPathLength = btnPath.getTotalLength();
			btnPath.style.transition = btnPath.style.WebkitTransition = 'none';
			btnPath.style.strokeDasharray = btnPathLength + ' ' + btnPathLength;
			btnPath.style.strokeDashoffset = btnPathLength;
			btnPath.getBoundingClientRect();
			btnPath.style.transition = btnPath.style.WebkitTransition = 'stroke-dashoffset .1s linear';
			let updatePath = function () {
				var scroll = win.scrollTop(),
					height = doc.height() - win.height(),
					progress = btnPathLength - (scroll * btnPathLength / height);
				btnPath.style.strokeDashoffset = progress;
			}
			updatePath();
			win.scroll(updatePath);	
		}
		
	})
	ui.plugin(LayoutAction);
	document.addEventListener('DOMContentLoaded', function(){
		$('body').LayoutAction();
	});
	$(document).on('click', '.btn_top', function(){
		var target = $(this.hash);
		$('html, body').animate({scrollTop: 0}, 500, function(){
			target.focus();
		});
		return false;
	});
})(window[LIB_NAME], jQuery);