
const win = $(window);
const Default = {};
const forEach = Array.prototype.forEach;
const activeClass = 'active';
const setClassName = 'hidden_decs';
const lineHeight = 96;
const NAME = "overflowText";

/**
 * @name StarRating
 * @selector [data-modules="overflowText"]'
 */
export default (function(core, $){
	const ui = core.ui;
	const Widget = ui.Widget;
	const OverflowText = Widget.extend({
		name : NAME,
		init : function (element, config){
			const _ = this;
			const options = _.options = $.extend({}, Default, config);
			_.element = $(element);
			Widget.fn.init.call(_, element, options);
			let height = _.element.outerHeight();
			if(height>lineHeight){
				_.element.addClass(setClassName);
				_._bindEvents()
			}
		},
		_bindEvents : function (){
			const _ = this;
			let btn = _.element.find('[data-element="button"]');
			btn.on('click', function(){
				var $this = $(this);
				if(_.element.hasClass(activeClass)){
					_.element.removeClass(activeClass);
					$this.children().text($this.data('openText'));
				}else{
					_.element.addClass(activeClass);
					$this.children().text($this.data('closeText'));
				}
			})
		}
	})
	ui.plugin(OverflowText);
})(window[LIB_NAME], jQuery);