
const win = $(window);
const Default = {};
const forEach = Array.prototype.forEach;
const activeClass = 'active';
const fixedClass = 'fix';
const SCORE = .5;
const NAME = 'starRating';

/**
 * @name StarRating
 * @selector [data-modules="starRating"]'
 */
export default (function(core, $){
	const ui = core.ui;
	const Widget = ui.Widget;
	const StarRating = Widget.extend({
		name : NAME,
		init : function(element, config){
			const _ = this;
			
			const options = _.options = $.extend({}, Default, config);
			Widget.fn.init.call(_, element, options);
			_.element = $(element);
			_.options.itemPosition = [];
			_.options.activeIndex = 0;
			_.options.isTouch = core.detect.isTouch;

			_._bindEvents()
		},
		_bindEvents : function(){
			const _ = this;
			if (_.options.isTouch) {
				_.element.on("touchstart", function(e){
					e.preventDefault();
					_._setPosition();
				})
				.on("touchmove", function(e){
					e.preventDefault();
					let target= $(this).find('>.star');
					let pageX = e.touches[0].pageX - this.offsetLeft;
					_._getPageX(pageX);
				})
				.on("touchend", function(e){
					e.preventDefault();
					let target = $(this).find('>.star'),
						pageX = e.originalEvent.changedTouches[0].pageX - this.offsetLeft,
						index = _._getPageX(pageX);
					target.removeClass(fixedClass);
					target.eq(index).addClass(fixedClass);
				})
			}else{
				_.element.on("mouseenter", '.star', function(e){
					e.preventDefault();
					_._setPosition();
					let target = $(this),
						index = target.index();
					_.rating(index);
				})
				.on("mouseleave", function(e){
					e.preventDefault();
					let targetIndex = $(this).find('>.star.fix').index();
					_.rating(targetIndex);
				})
				.on("click", '.star', function(e){
					e.preventDefault();
					let target = $(this);
					target.siblings().removeClass(fixedClass);
					target.addClass(fixedClass)
				});
			}			
		},
		_getPageX : function(x){
			const _ = this;
			let i;
			for (i = 0; i < _.options.itemPosition.length; i++) {
				let maxIndex = i+1;
				if( _.options.itemPosition[i] <= x && _.options.itemPosition[maxIndex] >= x){
					_.rating(i);
					break;
				}else if(i >= _.options.itemPosition.length-1){
					_.rating(i);
					break;
				}else if(x <= 0){
					_.rating(0);
					break;
				}
			}
			return i;
		},
		_setPosition : function(){
			const _ = this;
			let items = _.element.find('>.star');
			forEach.call(items, function (item, i) {
				_.options.itemPosition.push(item.offsetLeft);
				if($(item).hasClass(fixedClass)){
					_.options.activeIndex = $(item).index();
				}
			})
		},
		rating : function(index){
			const _ = this;
			let target = _.element.find('>.star'),
				scoreTxt = _.element.find('>.num');
			forEach.call(target, function (item, i) {
				if (index >= i ){
					_.options.activeIndex = i;
					item.classList.add(activeClass);
					scoreTxt.text(parseFloat(SCORE*(i+1)).toFixed(1))
				}else{
					_.options.activeIndex = i;
					item.classList.remove(activeClass);
				}
			})
		}
	})
	ui.plugin(StarRating);
})(window[LIB_NAME], jQuery);
