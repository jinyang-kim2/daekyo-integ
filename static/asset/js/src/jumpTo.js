/**
 * @name PageScrollNav
 * @selector [data-module="pageScrollNav"]
 * @example 
 * 		$('[data-module="jumpTo"]').jumpTo();
 */


const Default = {
	scrollWrap : $(document),
	nav : null,
	navMobile : null,
	conts : null,
	conts_pos : [],
	nav_pos : {},
	maxHeight : null,
	marginPc : 0,
    marginMobile : 0,
    contPadding : [100, 40]
};
const NAME = "jumpTo";

export default (function(core, $){
	const ui = core.ui;
	const Widget = ui.Widget;
	const JumpTo = Widget.extend({
		name : NAME,
		init : function (element, config){
			const _ = this;
			const options = _.options = $.extend({}, Default, config);
			let wrap = _.options.scrollWrap;
			_.element = $(element);
			Widget.fn.init.call(_, element, options);
			_.options.nav = _.element.find('a');
            _.options.conts = [];
            _.options.padding = (core.detect.mediaInfo.mode === 'mobile') ? _.options.marginMobile : _.options.marginPc;
            $(window).on("changeSize", function(e, mode){
                if(mode == "mobile"){
                    _.options.padding = _.options.marginMobile;
                }else{
                    _.options.padding = _.options.marginPc;
                }
            })

			$(window).on('scroll', function(){
				_._scrollCheck(wrap.scrollTop());
			})
			_.options.nav.on('click', function(e){
                e.preventDefault();
				var index = $(this).parent().index();
				$('html, body').stop().animate({scrollTop : _.options.conts[index] +1}, 500);
				return false;
			});
			_.options.maxHeight = $('body').prop('scrollHeight') - $(window).height();
			_.getContPos();
			$(window).on('resize load', function(){
				_.options.maxHeight = $('body').prop('scrollHeight') - $(window).height();
				_.getContPos();
			});
			
            
        },
        _scrollCheck : function (pos){
            const _=this;
            // console.log(pos)
			let max = _.options.conts.length,
				i;
			// 스크롤 위치 별 인덱스 반환
			for (i = 0 ; i < max ; i++){
				if (_.options.conts[i] >= pos) {
					i-=1;
					break;
				}
			};
			// console.log(_.options.maxHeight)
			if (i == max /*|| (_.options.maxHeight-10) <= pos*/){
				_._activesNav(max-1);
			}else{
				if(i >= 0){
					_._activesNav(i);
				}else if(i < 0){
					_._activesNav(0);
				}
            }
            _._navPos(pos);
        },
        _navPos : function (pos){
			const _ = this;
            let navPos = _.element.offset().top - _.options.padding;
			if (pos >= navPos ) {
                _.element.addClass('fixed');
			}else{
				_.element.removeClass('fixed');
			}
        },
        _activesNav : function (n){
            const _ = this;
			let navLists = _.options.nav.parent('li'),
				activeIndex = _.options.nav.parent('li.active').index();

			if (activeIndex != n && activeIndex == -1){
				navLists.eq(n).addClass('active');
			}else if(activeIndex != n){
				navLists.eq(n).addClass('active');
				navLists.eq(activeIndex).removeClass('active');
			};
		},
        getContPos : function () {
			const _ = this;
			_.options.conts = [];
            _.options.nav.each(function(n){
                _.options.conts.push($(this.hash))
            })
            for (let i = 0; i < _.options.conts.length; i++) {
                let pos = _.options.conts[i].offset().top - ((core.detect.mediaInfo.mode === 'mobile') ? _.options.contPadding[1] : _.options.contPadding[0]);
                _.options.conts[i] = pos;
			}
        }
	})
	ui.plugin(JumpTo);
	$(window).on('load', function () {
		$('[data-module="scrollNav"]').jumpTo();
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			if($(e.target.hash).find('[data-module="scrollNav"]').length){
				$('[data-module="scrollNav"]').jumpTo('refresh');
			}
		})
	});
})(window[LIB_NAME], jQuery);