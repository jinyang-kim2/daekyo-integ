$(function(){       
    // console.log(DiepUi.detect.isIE)
    // console.log(DiepUi.detect.ieVersion)
    
    $('[data-callayer]').on('click', function(e){
        e.preventDefault();
        var $this = $(this),
            target = $this.data('callayer');
        $('body').data('LayoutAction').controllLayers($this, target);
        return false;
        
    });
    $('[data-closelayer]').on('click', function(e){
        e.preventDefault();
        var $this = $(this),
            target = $this.data('closelayer');
        $('body').data('LayoutAction').closeLayers($this, target);
        
        return false;
        
    });
    if($('body').find('#bannerWrap').length !== 0) $('body').LayoutAction('commonBanner');
    $('body').LayoutAction('setFooterPadding');
    
    var page_name = $('#page-name').text();
    $('.btn_back').find('span').html(page_name);
})